#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ESP8266HTTPClient.h>
#include <Streaming.h>

#define TOKEN "<token>"
#define URL "<url>"

bool serverDisabled = false;
bool termometerEnabled = false;

ESP8266WebServer server(80);
OneWire oneWire(D1);
DallasTemperature DS18B20(&oneWire);

void handleRoot() {
  server.send(200, "text/html", "<form action='/update' method='POST'><input name='ssid' value='' placeholder='Nazwa sieci'><input name='pass' value='' placeholder='Haslo'><button>Zapisz</button></form>");
}

void setupWifi(String ssid, String password) {
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(D4, LOW);
    delay(200);
    digitalWrite(D4, HIGH);
    delay(800);
  }
}

void setupMeasurements(String ssid, String password) {
  Serial.print("ssid: ");
  Serial.println(ssid);
  Serial.print("password: ");
  Serial.println(password);
  setupWifi(ssid, password);
  DS18B20.begin();
  termometerEnabled = true;
  Serial.flush();
  delay(1000);
}

void handleUpdate() {
  String ssid = server.arg(0).substring(0, 31);
  String password = server.arg(1).substring(0, 31);
  
  EEPROM.put(0, ssid);
  EEPROM.put(32, password);
  
  serverDisabled = EEPROM.commit();
  if (serverDisabled) {
    server.send(200, "text/html", "Sukces!<br>nazwa sieci: " + ssid + "<br>haslo: " + password);
    delay(10000);
    WiFi.mode(WIFI_OFF);
    server.stop();
    delay(1000);
    setupMeasurements(ssid, password);
  } else {
    server.send(200, "text/html", "Błąd EEPROM!");
  }
}

void startSerwer() {
  if(WiFi.softAP("gien_term", 0, 9, false, 1)) {
    server.on("/", handleRoot);
    server.on("/update", handleUpdate);
    server.begin();
    Serial.println("Config server launched!");
  } else {
    Serial.println("Config server error!");
  }
  Serial.flush();
  delay(100);
}

void setup() {
  delay(1000);
  Serial.begin(115200);
  EEPROM.begin(64);
  pinMode(D4, OUTPUT);
  pinMode(D3, INPUT);
  digitalWrite(D4, LOW);
  delay(500);
  digitalWrite(D4, HIGH);

  serverDisabled = digitalRead(D3);
  if (serverDisabled) {
    String ssid;
    String password;

    EEPROM.get(0, ssid);
    EEPROM.get(32, password);
  
    setupMeasurements(ssid, password);
  } else {
    startSerwer();
  }
  delay(100);
}

float getTemperature() {
  float temp;
  do {
    DS18B20.requestTemperatures();
    temp = DS18B20.getTempCByIndex(0);
    delay(100);
  } while (temp == 85.0 || temp == (-127.0));
  return temp;
}

void sendTemperature() {
  float temperature = getTemperature();

  String body = "{\"temperature\":";
  body += temperature;
  body += ",\"magic\":\"";
  body += TOKEN;
  body += "\"}";
  
  HTTPClient http;
  http.begin(URL);
  http.addHeader("Content-Type", "application/json");
  http.POST(body);
  http.end();
}

void loop() {
  if(!serverDisabled) {
    server.handleClient();
  } else if (termometerEnabled) {
    sendTemperature();
    ESP.deepSleep(30000000);
  } else {
    delay(2000);
  }
}
